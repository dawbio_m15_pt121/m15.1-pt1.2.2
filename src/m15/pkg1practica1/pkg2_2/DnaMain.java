package m15.pkg1practica1.pkg2_2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class DnaMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        DnaMain myApp = new DnaMain();
        myApp.run();
    }

    /**
     * Function that prints the menu and returns the option chosen
     *
     * @return the selected option or -1 in case of error
     */
    private int printMenu() {
        int option = -1;

        System.out.println("\nChoose an option:");
        System.out.println("0.-Exit");
        System.out.println("1.-Reverse DNA sequence");
        System.out.println("2.-Count bases");
        System.out.println("3.-Most repeated base");
        System.out.println("4.-Least repeated base");

        System.out.print("\nOption: ");

        try {
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();
        } catch (Exception e) {
            System.out.println((char)27 + "[31m" + "Data error" + (char)27 + "[0m");
        }

        return option;
    }

    /**
     * Function that runs the app
     */
    private void run() {
        int option = 0;
        DnaFunction dnaFunct = new DnaFunction();
        FileReaders file = new FileReaders();
        ArrayList<String> dnaSequence_list = file.readSequence("src/m15/pkg1practica1/pkg2_2/dnaSequence.txt");
        String dnaSequence = String.join("", dnaSequence_list);
        dnaSequence = dnaSequence.toUpperCase();

        do {
            option = printMenu();
            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    System.out.print("DNA sequence: " + dnaSequence);
                    dnaFunct.reverseDNASequence(dnaSequence);
                    break;
                case 2:
                    System.out.print("DNA sequence: " + dnaSequence);
                    dnaFunct.printBases(dnaSequence);
                    break;
                case 3:
                    System.out.print("DNA sequence: " + dnaSequence);
                    dnaFunct.mostRepeatedBase(dnaSequence);
                    break;
                case 4:
                    System.out.print("DNA sequence: " + dnaSequence);
                    dnaFunct.leastRepeatedBase(dnaSequence);
                    break;
                default:
                    System.out.println((char)27 + "[31m" + "Not a valid option" + (char)27 + "[0m");
                    break;
            }
        } while (option != 0);
    }
}
