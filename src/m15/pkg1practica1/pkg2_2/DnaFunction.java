package m15.pkg1practica1.pkg2_2;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author tarda
 */
public class DnaFunction {

    /**
     * Function that returns the reverse of the DNA sequence
     * @param DNA
     */
    public void reverseDNASequence(String DNA) {
        /*
        //Option 1 -> convert string to char array
        char[] letters = DNA.toCharArray();

        for (int i = letters.length - 1; i >= 0; i--) {
            System.out.print(letters[i]);
        }
                
        //Option 2 -> convert string to chars
        for (int i = DNA.length()-1; i >= 0; i--) {
            char letter = DNA.charAt(i);
            System.out.print(letter);
        }*/
        
        //Option 3 -> method reverse()
        String[] letters = DNA.split("");
        
        Collections.reverse(Arrays.asList(letters));
        String reverse = String.join("", letters);
                      
        System.out.print("\nReverse DNA sequence: ");
        System.out.println(reverse);
    }

    /**
     * Function that calculates the count of each base and returns it in an array
     * @param DNA
     * @return array counter
     */
    public Integer[] countBases(String DNA) {
        int count_a = 0;
        int count_t = 0;
        int count_c = 0;
        int count_g = 0;
        Integer[] counter = {count_a, count_t, count_c, count_g};

        for (int i = 0; i < DNA.length(); i++) {
            char letter = DNA.charAt(i);
            switch (letter) {
                case 'A':
                    counter[0]++;
                    break;
                case 'T':
                    counter[1]++;
                    break;
                case 'C':
                    counter[2]++;
                    break;
                case 'G':
                    counter[3]++;
                    break;
            }
        }
        
        return counter;
    }
    
    /**
     * Function that prints the count of each base
     * @param DNA 
     */
    public void printBases(String DNA){
        Integer[] counter = countBases(DNA);
        System.out.println("\nCount of bases:");
        System.out.println("No. A: " + counter[0]);
        System.out.println("No. T: " + counter[1]);
        System.out.println("No. C: " + counter[2]);
        System.out.println("No. G: " + counter[3]);
    }
        
    /**
     * Function that prints the most repeated base
     * @param DNA 
     */
    public void mostRepeatedBase(String DNA){
        char[] letters = {'A', 'T', 'C', 'G'};
        Integer[] counter = countBases(DNA);
        int max = 0;
        int posMax;
        
        /*
        //Option 1
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] > max){
                max = counter[i];
                posMax = i;
            } 
        }*/
        
        //Option 2 -> method max()
        max = Collections.max(Arrays.asList(counter));
        System.out.println("\nMost repeated base(s):");
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] == max){
                System.out.println("Base: " + letters[i] + ", Num of repeats: " + max);
            }
        }
    }
    
    /**
     * Function that prints the least repeated base
     * @param DNA 
     */
    public void leastRepeatedBase(String DNA){
        char[] letters = {'A', 'T', 'C', 'G'};
        Integer[] counter = countBases(DNA);
        int min = 0;
        int posMin;
        
        /*
        //Option 1
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] < min){
                min = counter[i];
                posMin = i;
            } 
        }*/
        
        //Option 2 -> method min()
        min = Collections.min(Arrays.asList(counter));
        
        System.out.println("\nLeast repeated base(s):");
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] == min){
                System.out.println("Base: " + letters[i] + ", Num of repeats: " + min);
            }
        }
    }
}
